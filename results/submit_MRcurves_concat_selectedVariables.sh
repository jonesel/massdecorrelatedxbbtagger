#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --mem=8G
#SBATCH --output=slurm_selectedVariables.out
#SBATCH --error=slurm_selectedVariables.err

module load GCC/8.3.0 OpenMPI/3.1.4 TensorFlow/2.1.0-Python-3.7.4 matplotlib/3.1.1-Python-3.7.4 PyTorch/1.4.0-Python-3.7.4 Anaconda3
echo "I am running the MRcurves for the selected variables tagger."

python MRcurves_concat_selectedVariables.py

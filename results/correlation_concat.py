import argparse
import os,glob,h5py,shutil
import tensorflow as tf
from tensorflow.keras import backend as K
import tensorflow.keras as keras
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Dense, Input, Dropout, Activation, BatchNormalization
from tensorflow.keras.optimizers import SGD,Adam
import numpy as np
import pandas as pd
import h5py
from pandas import HDFStore,DataFrame
import matplotlib
matplotlib.use('Agg')
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score
from matplotlib.backends.backend_pdf import PdfPages
import seaborn as sns
from string import ascii_letters

# Get test data
f=h5py.File("/storage/epp2/phrsbc/trainstd_ZPrime.h5",'r')
test=pd.DataFrame(f['train'])
#list1=['weight',1,2,3,4,'mass','pt','eta',8,9,10,11,12,13,14,15,16,'Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12',
#'KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',31,32,33,34,35,36,'DijetSignal','HiggsSignal','TopSignal']
#test=pd.DataFrame(test, columns=list1)

# Slice and rename coloumns
print("Slicing")
slices = [5,6,7,17,18,19,20,21,22,23,24,25,26,27,28,29,30,37]
#slices = [5,6,7,17,37]
test_new = test.iloc[:, slices]
#test_new.rename(columns= {test_new.columns[0]: "Mass", test_new.columns[1]: "Split12", test_new.columns[2]: "Split23", test_new.columns[3]: "Qw", test_new.columns[4]: "PlanarFlow", test_new.columns[5]: "Angularity", test_new.columns[6]: "Aplanarity", test_new.columns[7]: "ZCut12", test_new.columns[8]: "KtDR", test_new.columns[9]: "C2", test_new.columns[10]: "D2", test_new.columns[11]: "e3", test_new.columns[12]: "Tau21_wta", test_new.columns[13]: "Tau32_wta", test_new.columns[14]: "FoxWolfram20",test_new.columns[15]: "Class"}, inplace = True)
#test_new=test_new.rename({})
#print(test_new.keys())

# Build Dijets Df
ddf = test_new[test_new[17] == 1]
slices = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
test_ddf = ddf.iloc[:, slices]

# Build hdf Df
hdf = test_new[test_new[17] == 0]
test_hdf = hdf.iloc[:, slices]

# Set up the dijets correlation matrix
print('Building Dijets Correlation matrix')
correlations =  test_ddf.corr()
mask = np.triu(np.ones_like(correlations, dtype=bool))
cmap = sns.diverging_palette(240, 10, n=9, as_cmap=True)
# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(12, 12))
sns.heatmap(correlations, annot=True, square=True, fmt='.2f', vmin=-1, vmax=1, cmap=cmap,
            center=0, linewidths=2, linecolor='white', cbar_kws= {'orientation': 'vertical', 'ticks':2*(np.divide(range(-5,6),10))})
#plt.title("Linear correlation between input variables (Dijets)")
#plt.xlabel("Features")
#plt.ylabel("Features")
#fix cut off on bottom and top boxes
b, t = plt.ylim() 
b += 0.5 
t -= 0.5 
plt.ylim(b, t)
pp=PdfPages('dijetsCorrelation_ZPrime.pdf')
plt.savefig(pp,format='pdf')
pp.close()
print('Saved ROC Curves to dijetsCorrelation_ZPrime.pdf')
print(correlations)


# Set up the higgs correlation matrix
print('Building Correlation matrix')
correlations1 =  test_hdf.corr()
mask = np.triu(np.ones_like(correlations1, dtype=bool))
cmap = sns.diverging_palette(240, 10, n=9, as_cmap=True)
# Set up the matplotlib figure
f, ax = plt.subplots(figsize=(12, 12))
sns.heatmap(correlations1, annot=True, square=True, fmt='.2f', vmin=-1, vmax=1, cmap=cmap,
            center=0, linewidths=2, linecolor='white', cbar_kws= {'orientation': 'vertical', 'ticks':2*(np.divide(range(-5,6),10))})
print(correlations1)
#plt.title("Linear correlation between input variables (Higgs)")
#plt.xlabel("Features")
#plt.ylabel("Features")
#fix cut off on bottom and top boxes
b, t = plt.ylim() 
b += 0.5 
t -= 0.5 
plt.ylim(b, t)
pp2 = PdfPages('higgsCorrelation_ZPrime.pdf')
plt.savefig(pp2, format='pdf')
pp2.close()
print('Saved ROC Curves to higgsCorrelation_ZPrime.pdf')









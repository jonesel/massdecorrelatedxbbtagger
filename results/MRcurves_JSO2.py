import argparse
import os,glob,h5py,shutil
import tensorflow as tf
from tensorflow.keras import backend as K
import tensorflow.keras as keras
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Dense, Input, Dropout, Activation, BatchNormalization
from tensorflow.keras.optimizers import SGD,Adam
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score
from matplotlib.backends.backend_pdf import PdfPages

# Directory with training output folders
path=('/storage/epp2/phrsbc/XbbOutput_SelectedVariables/*')

# Get test data
print("Loading test data")
test=pd.read_hdf("/storage/epp2/phrsbc/testData_concat.h5", mode="r")

# 100 Epoch Models
labels={'train_0':'$D_{Xbb}$','train_select20':'$D_{Xbb}+PlanarFlow$','train_select21':'$D_{Xbb}+Angularity$','train_select22':'$D_{Xbb}+Aplanarity$','train_select30':'$D_{Xbb}+FoxWolfram20$'}
colours={'train_0':'black','train_select20':'tab:purple','train_select21':'tab:brown','train_select22':'tab:pink','train_select30':'red'}
fatjet={'train_0':(test.iloc[:,[6,7]]),'train_select20':(test.iloc[:,[6,7,20]]),'train_select21':(test.iloc[:,[6,7,21]]),'train_select22':(test.iloc[:,[6,7,22]]),'train_select30':(test.iloc[:,[6,7,30]])}

# Get the input features
subjet0=test.iloc[:, [ 8, 9,10]]
subjet1=test.iloc[:, [11,12,13]]
subjet2=test.iloc[:, [14,15,16]]

test_y=test.iloc[:,[38]]
test_w=test.iloc[:,0] # use weight - should RW to flat distribution in pT

modelPredictions=pd.DataFrame()


# Find files
for folder in glob.glob(path) :
    id = str(folder.split("/")[-1])
    if id in fatjet :
        if id == "train_0" : 
            bbtagger = glob.glob((path+id+'/*DL1r.h5'))
        else : 
            bbtagger = glob.glob((path+id+'/*DL1r_best.h5'))
        print("Tagger: ",bbtagger)

        # Load Model
        print("Loading model for "+id)
        model = tf.keras.models.load_model(bbtagger[0])
        print("Testing model for "+id)
        predictions = {id:(model.predict(x=[fatjet[id],subjet0,subjet1,subjet2])[:,1])}
        df = pd.DataFrame(predictions)
        modelPredictions=pd.concat((modelPredictions,df), axis=1)

eff=[]
originalY=[]

# Calculate fpr and tpr of DXbb
print('Calculating DXbb ROC Curve')
fpr, tpr, _ = roc_curve(test_y, modelPredictions['train_0'], sample_weight=test_w)
info=np.column_stack((tpr,np.power(fpr,-1)))
for i in range(100):
    j=i/100.0
    eff.append(j)
    a=info[abs(info[:,0]-j)==abs(info[:,0]-j).min()][0,1]
    originalY.append(a)

# Calculate fpr and tpr of others
count = 0 
for id in modelPredictions : 
    print('Calculating ROC Curve for '+id)
    fpr_1, tpr_1, _1 = roc_curve(test_y, modelPredictions[id], sample_weight=test_w)
    info=np.column_stack((tpr_1,np.power(fpr_1,-1)))
    newY=[]
    for i in range(100):
        j=i/100.0
        b=info[abs(info[:,0]-j)==abs(info[:,0]-j).min()][0,1]
        newY.append(b)

    # compute ratio of fpr to DXbb
    print('Calculation ratio of '+id+' to DXbb')
    ratio=np.divide(newY,originalY)

    ax1=plt.subplot(2,1,1)
    
    if count == 0 : 
        plt.axvline(0.4, color='k', lw=0.8)
        plt.yscale("log", nonposy="clip")
        plt.xlim(0.1,1)
        plt.ylim(1, 10**4)
        ax1.minorticks_on()
        plt.grid(b=True, which='both', axis='x')
        plt.gca().set_xticklabels(['']*10)
        plt.ylabel('Multijet Rejection')

    print('Plotting ROC Curves')
    plt.plot(tpr,np.power(fpr,-1), label=labels[id], color=colours[id])
 
    print('Plotting ratios of taggers to DXbb')
    ax2=plt.subplot(2,1,2)

    if count == 0 : 
        plt.axvline(0.4, color='k', lw=0.8)
        plt.xlim(0.1,1)
        plt.ylim(0,4.4)
        plt.xlabel('Higgs Efficiency')
        plt.ylabel('Ratio to $D_{Xbb}$')
        ax2.minorticks_on()
        plt.grid(b=True, which='both', axis='x')

    plt.plot(eff, ratio, color=colours[id])
    count += 1

# Add a legend
ax1.legend(loc='upper center', bbox_to_anchor=(0.49,1.33), fancybox=True, shadow=True, ncol=2)

pp = PdfPages('Comparison_JSO2.pdf')
plt.subplots_adjust(wspace=0, hspace=0.1)
plt.savefig(pp, format='pdf')
pp.close()
print("Figures Saved to Comparison_JSO2.pdf")


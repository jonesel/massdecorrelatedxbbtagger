import argparse
import os,glob,h5py,shutil
import numpy as np
import pandas as pd

# Get test data
print("Loading test data")
test_file=h5py.File("/storage/epp2/atlas/Testing/trainstd.h5","r")
test_file2=h5py.File("/storage/epp2/phrsbc/trainstd.h5","r")
test=test_file.get("test")
test2=test_file2.get("test")
valid=test_file.get("valid")
train=test_file.get("train")

list1=['weight','relativeDeltaRToVRJet_1','relativeDeltaRToVRJet_2','relativeDeltaRToVRJet_3','dsid','mass','pt','eta','DL1r_pu_1','DL1r_pc_1','DL1r_pb_1','DL1r_pu_2','DL1r_pc_2','DL1r_pb_2','DL1r_pu_3','DL1r_pc_3','DL1r_pb_3','Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',31,32,33,34,35,36,'DijetSignal','HiggsSignal','TopSignal']

print("Saving train as dataframe")
train=pd.DataFrame(train, columns=list1)
print(train.shape)
print("Saving valid as dataframe")
valid=pd.DataFrame(valid, columns=list1)
print(valid.shape)
print("Saving test as dataframe")
test=pd.DataFrame(test, columns=list1)
print(test.shape)

print("\n\nSaving dijets as dataframe")
dijets=pd.concat([train, valid, test], axis=0)
print(dijets.shape)
dijets=dijets[dijets.DijetSignal == 1]
print(dijets.shape)

print("Saving test as dataframe")
test2=pd.DataFrame(test2, columns=list1)
print(test2.shape)
test2=pd.concat([test2, dijets], axis=0)
print(test2.shape)

print("Applying Mass, eta and pT cuts")
cuts=((np.where((test2['mass']>76) & (test2['mass']<146) & (abs(test2['eta'])<2.0) & (test2['pt']>500)))[0])
test2=test2.iloc[cuts]
print(test2.shape)

#Save selected jets to h5 file
test2.to_hdf('/storage/epp2/phrsbc/testData_concat.h5', 'df', mode='w')

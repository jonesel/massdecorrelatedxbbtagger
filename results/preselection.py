import argparse
import os,glob,h5py,shutil
import numpy as np
import pandas as pd

# Get test data
print("Loading test data")
test_file=h5py.File("/storage/epp2/phrsbc/trainstd.h5","r")
test=test_file.get("test")

# Column headings
list1=['weight','relativeDeltaRToVRJet_1','relativeDeltaRToVRJet_2','relativeDeltaRToVRJet_3','dsid','mass','pt','eta','DL1r_pu_1','DL1r_pc_1','DL1r_pb_1','DL1r_pu_2','DL1r_pc_2','DL1r_pb_2','DL1r_pu_3','DL1r_pc_3','DL1r_pb_3','Split12','Split23','Qw','PlanarFlow','Angularity','Aplanarity','ZCut12','KtDR','C2','D2','e3','Tau21_wta','Tau32_wta','FoxWolfram20',31,32,33,34,35,36,'DijetSignal','HiggsSignal','TopSignal']

# Create dataframe with test data
print("Saving test as dataframe")
test=pd.DataFrame(test, columns=list1)
print(test)

# Apply cuts
print("Applying Mass, eta and pT cuts")
cuts=((np.where((test['mass']>76) & (test['mass']<146) & (abs(test['eta'])<2.0) & (test['pt']>500)))[0])
test_cuts=test.iloc[cuts]
print(test_cuts)

# Save selected jets to h5 file
test.to_hdf('/storage/epp2/phrsbc/testData.h5', 'df', mode='w')
print("New file, 'testData.h5' saved.")

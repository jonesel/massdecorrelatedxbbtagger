import argparse
import os,glob,h5py,shutil
import tensorflow as tf
from tensorflow.keras import backend as K
import tensorflow.keras as keras
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Dense, Input, Dropout, Activation, BatchNormalization
from tensorflow.keras.optimizers import SGD,Adam
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score
from matplotlib.backends.backend_pdf import PdfPages

# Directory with training output folders
path=('/storage/epp2/phrsbc/XbbOutput_Epochs/')

# Get test data
print("Loading test data")
test=pd.read_hdf("/storage/epp2/phrsbc/testData_concat.h5", mode="r")

# 100 Epoch Models
labels={'train_0':'$D_{Xbb}$','train_select17232426':'$D_{Xbb}+Split12+ZCut12+KtDR+D2$'}
colours={'train_0':'black','count1':'tab:blue','count2':'tab:orange','count3':'tab:green','count4':'tab:purple','count5':'tab:brown','count6':'tab:pink','count7':'tab:grey','count8':'tab:olive','count9':'tab:cyan','count10':'springgreen','count11':'midnightblue','count12':'yellow','count13':'fuscia','count14':'red'}
fatjet={'train_0':(test.iloc[:,[6,7]]),'train_select17232426':(test.iloc[:,[6,7,17,23,24,26]])}

# Get the input features
subjet0=test.iloc[:, [ 8, 9,10]]
subjet1=test.iloc[:, [11,12,13]]
subjet2=test.iloc[:, [14,15,16]]

test_y=test.iloc[:,[38]]
test_w=test.iloc[:,0] # use weight - should RW to flat distribution in pT

modelPredictions=pd.DataFrame()


# Find files
bbtagger = glob.glob((path+'train_0/*DL1r.h5'))
# Load Model
print('Loading model for DXbb')
model = tf.keras.models.load_model(bbtagger[0])
print('Testing model for DXbb')
predictions = {'train_0':(model.predict(x=[fatjet['train_0'],subjet0,subjet1,subjet2])[:,1])}

for filename in os.listdir(path+'train_select17232426/') : 
    if filename.endswith('.json') or filename.endswith('.log') or filename.endswith('.pdf') or filename.endswith('weights.h5') : 
        continue
    elif filename.endswith('.h5') : 
        bbtagger = glob.glob((path+'train_select17232426/'+filename))
    else :
        continue

    print('Tagger: ',bbtagger)
    print('Loading model for '+filename)
    model = tf.keras.models.load_model(bbtagger[0])
    print('Testing model for '+filename)
    predictions[filename]=model.predict(x=[fatjet['train_select17232426'],subjet0,subjet1,subjet2])[:,1]
    print(predictions)

df = pd.DataFrame(predictions)
modelPredictions=pd.concat((modelPredictions,df), axis=1)
print(modelPredictions)

eff=[]
originalY=[]

# Calculate fpr and tpr of DXbb
print('Calculating DXbb ROC Curve')
fpr, tpr, _ = roc_curve(test_y, modelPredictions['train_0'], sample_weight=test_w)
info=np.column_stack((tpr,np.power(fpr,-1)))
for i in range(100):
    j=i/100.0
    eff.append(j)
    a=info[abs(info[:,0]-j)==abs(info[:,0]-j).min()][0,1]
    originalY.append(a)

fig,(ax1,ax2)=plt.subplots(2)
ax1.set_xlim([0.1,1])
ax1.set_ylim([1,10**4])
ax1.set_yscale('log')
ax1.minorticks_on()
ax1.grid(b=True, which='major', axis='both')
ax1.set_xticklabels(['']*10)
ax1.set(ylabel='Multijet Rejection')
ax1.plot(tpr,np.power(fpr,-1),label='DXbb',color='k')

ax2.axhline(1.0, color='k', lw=0.8)
ax2.set_xlim([0.1,1])
ax2.set_ylim([0,4.4])
ax2.set(xlabel='Higgs Efficiency',ylabel='Ratio to $D_{Xbb}$')
ax2.minorticks_on()
ax2.grid(b=True,which='major',axis='both')

# Calculate fpr and tpr of others
count = 0 
for id in modelPredictions :
    print(count)
    if count == 0 :
        count += 1
        continue

    print('Calculating ROC Curve for '+id)
    fpr, tpr, _ = roc_curve(test_y, modelPredictions[id], sample_weight=test_w)
    info=np.column_stack((tpr,np.power(fpr,-1)))
    newY=[]
    for i in range(100):
        j=i/100.0
        b=info[abs(info[:,0]-j)==abs(info[:,0]-j).min()][0,1]
        newY.append(b)
        
    # compute ratio of fpr to DXbb
    print('Calculating ratio of '+id+' to DXbb')
    ratio=np.divide(newY,originalY)
    
    print('Plotting ROC Curve: '+id)
    ax1.plot(tpr,np.power(fpr,-1),label=str(id),color=colours['count'+count])
    print('Plotting ratios of taggers to DXbb')
    ax2.plot(eff, ratio)
        
    count += 1

# Add a legend
ax1.legend(loc='upper center', bbox_to_anchor=(0.49,1.33), fancybox=True, shadow=True, ncol=2)

fig.savefig('Comparison_allEpochs.pdf')
plt.close()

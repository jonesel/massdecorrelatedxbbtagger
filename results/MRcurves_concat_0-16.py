import argparse
import os,glob,h5py,shutil
import tensorflow as tf
from tensorflow.keras import backend as K
import tensorflow.keras as keras
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Dense, Input, Dropout, Activation, BatchNormalization
from tensorflow.keras.optimizers import SGD,Adam
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score
from matplotlib.backends.backend_pdf import PdfPages

# Directory with training output folders
path=('/storage/epp2/phrsbc/XbbOutput_100epochs/*')

# Get test data
print("Loading test data")
test=pd.read_hdf("/storage/epp2/phrsbc/testData_concat.h5", mode="r")

# 100 Epoch Models
labels={'train_0':'$D_{Xbb}$', 'train_14':'JSS$'}
colours={'train_0':'black',  'train_14':'red'}
fatjet={'train_0':(test.iloc[:,[6,7]]), 'train_14':(test.iloc[:, [ 6,7,17,18,19,20,21,22,23,24,25,26,27,28,29,30]])}

# Get the input features
subjet0=test.iloc[:, [ 8, 9,10]]
subjet1=test.iloc[:, [11,12,13]]
subjet2=test.iloc[:, [14,15,16]]

test_y=test.iloc[:,[38]]
test_w=test.iloc[:,0] # use weight - should RW to flat distribution in pT

modelPredictions=pd.DataFrame()


# Find files
for folder in glob.glob(path):
    id=str(folder.split("/")[-1])
    if id in fatjet:
        if id=="train_0" : 
            bbtagger=glob.glob((path+id+'/*DL1r.h5'))
        else :
            bbtagger=glob.glob((path+id+'/*DL1r_best.h5'))
        print("bbtagger:",bbtagger)

        # Load Model
        print("Loading model for "+id)
        model = tf.keras.models.load_model(bbtagger[0])
        print("Testing model for "+id)
        predictions={id:(model.predict(x=[fatjet[id],subjet0,subjet1,subjet2])[:,1])}
        df=pd.DataFrame(predictions)
        modelPredictions=pd.concat((modelPredictions,df), axis=1)

eff=[]
originalY=[]

# Calculate fpr and tpr of dxbb
fpr, tpr, _ =roc_curve(test_y, modelPredictions['train_0'], sample_weight=test_w)
info=np.column_stack((tpr,np.power(fpr,-1)))
for i in range(100):
    j=i/100.0
    eff.append(j)
    a=info[abs(info[:,0]-j)==abs(info[:,0]-j).min()][0,1]
    originalY.append(a)

#calculate fpr and tpr of other taggers
count=0

for id in modelPredictions:
    print('Calculating ' + id + ' ROC Curve')
    fpr, tpr, _ = roc_curve(test_y, modelPredictions[id], sample_weight=test_w)
    info=np.column_stack((tpr,np.power(fpr,-1)))
    newY=[]
    for i in range(100):
        j=i/100.0
        b=info[abs(info[:,0]-j)==abs(info[:,0]-j).min()][0,1]
        newY.append(b)
    # compute ratio of fpr to dxbb
    ratio=np.divide(newY,originalY)

    ax1=plt.subplot(2,1,1)
    if count == 0:
        plt.axvline(0.4, color='k', lw=0.8)
        plt.yscale("log", nonposy="clip")
        plt.xlim(0.1,1)
        plt.ylim(1, 10**4)
        ax1.minorticks_on()
        plt.grid(b=True, which='both', axis='x')
        plt.gca().set_xticklabels(['']*10)
        plt.ylabel('Multijet Rejection')

    plt.plot(tpr,np.power(fpr,-1), label=labels[id], color=colours[id])
    print(labels[id])
    for i in range(0,len(tpr)):
        if (tpr[i] < 0.41) & (tpr[i] > 0.399):
            print(tpr[i])
            print(np.power(fpr[i],-1))
            print()

    ax2=plt.subplot(2,1,2)
    if count == 0:
        plt.axvline(0.4, color='k', lw=0.8)
        plt.xlim(0.1,1)
        plt.ylim(0,4.4)
        plt.xlabel('Higgs Efficiency')
        plt.ylabel('Ratio to $D_{Xbb}$')
        ax2.minorticks_on()
        plt.grid(b=True, which='both', axis='x')
    plt.plot(eff, ratio, color=colours[id])
    count+=1

pp = PdfPages('Comparison_concat_0-16.pdf')
plt.subplots_adjust(wspace=0, hspace=0.1)
plt.savefig(pp, format='pdf')
pp.close()
print("Figures Saved to Comparison_concat_0-16.pdf")


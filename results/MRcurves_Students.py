import argparse
import os,glob,h5py,shutil
import tensorflow as tf
from tensorflow.keras import backend as K
import tensorflow.keras as keras
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Dense, Input, Dropout, Activation, BatchNormalization
from tensorflow.keras.optimizers import SGD,Adam
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score
from matplotlib.backends.backend_pdf import PdfPages

# Directory with training output folders
path=('/storage/epp2/phrsbc/XbbOutput_Students/')

# Get test data
print("Loading test data")
test=pd.read_hdf("/storage/epp2/phrsbc/testData.h5", mode="r")

# 100 Epoch Models
labels={'2035084':'$D_{Xbb}$', '2035071':'JSS'}
colours={'2035084':'black', '2035071':'red'}
fatjet={'2035084':(test.iloc[:,[6,7]]),'2035071':(test.iloc[:, [ 6,7,17,18,19,20,21,22,23,24,25,26,27,28,29,30]])}

# Get the input features
subjet0=test.iloc[:, [ 8, 9,10]]
subjet1=test.iloc[:, [11,12,13]]
subjet2=test.iloc[:, [14,15,16]]

test_y=test.iloc[:,[38]]
test_w=test.iloc[:,0] # use weight - should RW to flat distribution in pT

modelPredictions=pd.DataFrame()


# Find files
bbtagger_DXbb=glob.glob((path+'bbtagger_AdmStd_DL1r_2035084.h5'))
bbtagger_JSS=glob.glob((path+'bbtagger_AdmStd_DL1r_best_2035071.h5'))
print("DXbb:",bbtagger_DXbb)
print("JSS:",bbtagger_JSS)

# Load Model
print("Loading model for DXbb")
model_DXbb=tf.keras.models.load_model(bbtagger_DXbb[0])
print("Testing model for DXbb")
predictions_DXbb={'2035084':(model_DXbb.predict(x=[fatjet['2035084'],subjet0,subjet1,subjet2])[:,1])}
df_DXbb=pd.DataFrame(predictions_DXbb)
modelPredictions=pd.concat((modelPredictions,df_DXbb), axis=1)

# Load Model
print("Loading model for JSS")
model_JSS=tf.keras.models.load_model(bbtagger_JSS[0])
print("Testing model for JSS")
predictions_JSS={'2035071':(model_JSS.predict(x=[fatjet['2035071'],subjet0,subjet1,subjet2])[:,1])}
df_JSS=pd.DataFrame(predictions_JSS)
modelPredictions=pd.concat((modelPredictions,df_JSS), axis=1)

eff=[]
originalY=[]

#Calculate fpr and tpr of dxbb
print('Calculating DXbb ROC Curve')
fpr, tpr, _  = roc_curve(test_y, modelPredictions['2035084'], sample_weight=test_w)
info=np.column_stack((tpr,np.power(fpr,-1)))
for i in range(100):
    j=i/100.0
    eff.append(j)
    a=info[abs(info[:,0]-j)==abs(info[:,0]-j).min()][0,1]
    originalY.append(a)

print('Calculating JSS ROC Curve')
fpr_1, tpr_1, _1 = roc_curve(test_y, modelPredictions['2035071'], sample_weight=test_w)
info=np.column_stack((tpr_1,np.power(fpr_1,-1)))
newY=[]
for i in range(100):
    j=i/100.0
    b=info[abs(info[:,0]-j)==abs(info[:,0]-j).min()][0,1]
    newY.append(b)

# compute ratio of fpr to dxbb
print('Calculating ratio of JSS to DXbb')
ratio=np.divide(newY,originalY)

ax1=plt.subplot(2,1,1)

plt.axvline(0.4, color='k', lw=0.8)
plt.yscale("log", nonposy="clip")
plt.xlim(0.1,1)
plt.ylim(1, 10**4)
ax1.minorticks_on()
plt.grid(b=True, which='both', axis='x')
plt.gca().set_xticklabels(['']*10)
plt.ylabel('Multijet Rejection')

print('Plotting DXbb Roc Curve')
plt.plot(tpr,np.power(fpr,-1), label=labels['2035084'], color=colours['2035084'])
print('Plotting JSS Roc Curve')
plt.plot(tpr_1,np.power(fpr_1,-1), label=labels['2035071'], color=colours['2035071'])

print('Plotting ratio of JSS to DXbb')
ax2=plt.subplot(2,1,2)
plt.axvline(0.4, color='k', lw=0.8)
plt.xlim(0.1,1)
plt.ylim(0,4.4)
plt.xlabel('Higgs Efficiency')
plt.ylabel('Ratio to $D_{Xbb}$')
ax2.minorticks_on()
plt.grid(b=True, which='both', axis='x')
plt.plot(eff, ratio, color=colours['2035071'])

pp = PdfPages('Comparison_Students.pdf')
plt.subplots_adjust(wspace=0, hspace=0.1)
plt.savefig(pp, format='pdf')
pp.close()
print("Figure saved to Comparison_Students.pdf")


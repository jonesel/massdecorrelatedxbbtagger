import argparse,math,os,glob,h5py
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
args = parser.parse_args()
import numpy as np
import tensorflow.keras as keras

def load_Data(data,style,nJets):
  load_f=h5py.File(data,"r")
  load_data=load_f.get(style)


# make an array with length = to input data and 3 columns to define the category
# define the category of each sample 
#   Dijet = [1,0,0]
#   Hbb   = [0,1,0]
#   Top   = [0,0,1]
# load_data.shape[0] = number of jets

  print(data)
  print(load_data.shape)

  if "Hbb" in data:
    y=np.full((load_data.shape[0],),1,dtype=int)
    load_y=keras.utils.to_categorical(y, num_classes=3)
  if "Top" in data:
    y=np.full((load_data.shape[0],),2,dtype=int)
    load_y=keras.utils.to_categorical(y, num_classes=3)
  if "Dijets" in data:
    y=np.full((load_data.shape[0],),0,dtype=int)
    load_y=keras.utils.to_categorical(y, num_classes=3)

  load_fdata=np.hstack((load_data,load_y)) # flipped original order so numbering in flatten is valid
  print("First entry")
  print(load_fdata[0])
  #Nominalize sum of weight into total samples, change 2M according to samples
  #load_fdata[:,3]=load_fdata[:,3]*2000000/np.sum(load_fdata[:,3])
  # WHERE DOES NUMBER 2M come from?
  # FIXME first magic number: if weight is not first feature in flatten script, this goes off the rails
  # REWEIGHT so average weight = 1 in smallest sample, rest match integral
  integral = np.sum(load_fdata[:,0])
  print("Reweight " + str(load_data.shape[0]) + " jets with integral of " + str(integral) + " to " + str(nJets))
  load_fdata[:,0]=load_fdata[:,0]*nJets/integral
  return load_fdata

outname="train.h5" #Prepare training
#outname="test.h5" #Prepare testing
save_f = h5py.File("../../PrepareData/"+outname, 'w')
files=sorted(glob.glob(args.path+"/*.h5"))
print("Have " + str(len(files)) + " files")
if len(files) != 2:
  exit()


#Prepare training samples
# NOT robust - should ensure file is correct one, not assume ordering
# Higgs is smallest sample, rewight event weights to average at unity
#  - could be done dynamically...
hFile=h5py.File(files[1],"r")
nJetsTrain = hFile.get("train").shape[0]
nJetsValid = hFile.get("valid").shape[0]
nJetsTest  = hFile.get("test").shape[0]
hFile.close()
print("nJetsTrain " + str(nJetsTrain))
print("nJetsValid " + str(nJetsValid))
print("nJetsTest  " + str(nJetsTest))
# Dijets is weighted to give the same integral
print("\n Prepare Train \n")
train_Dijets=load_Data(files[0],"train",nJetsTrain)
train_Hbb=load_Data(files[1],"train",nJetsTrain)
#train_Top=load_Data(files[2],"train",nJetsTrain)
#train_data=np.vstack((train_Dijets,train_Hbb,train_Top))
train_data=np.vstack((train_Dijets,train_Hbb))
save_f.create_dataset("train",data=train_data)

print("\n Prepare Valid \n")
valid_Dijets=load_Data(files[0],"valid",nJetsValid)
valid_Hbb=load_Data(files[1],"valid",nJetsValid)
#valid_Top=load_Data(files[2],"valid",nJetsValid)
#valid_data=np.vstack((valid_Dijets,valid_Hbb,valid_Top))
valid_data=np.vstack((valid_Dijets,valid_Hbb))
save_f.create_dataset("valid",data=valid_data)

#Prepare testing samples
# IS THIS MEANT TO BE COMMENTED OUT??
# RUN IT FOR NOW
'''
'''
print("\n Prepare Test \n")
test_Dijets=load_Data(files[0],"test",nJetsTest)
test_Hbb=load_Data(files[1],"test",nJetsTest)
#test_Top=load_Data(files[2],"test",nJetsTest)
#test_data=np.vstack((test_Dijets,test_Hbb,test_Top))
test_data=np.vstack((test_Dijets,test_Hbb))
save_f.create_dataset("test",data=test_data)



 













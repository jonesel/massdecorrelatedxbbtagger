import numpy as np
import pandas as pd
import glob,h5py
import argparse,math,os
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
args = parser.parse_args()

# open output of calculateMean script
Mean="../../PrepareData/meanstd.h5"
meanFile=h5py.File(Mean,"r")
mean_vector=meanFile.get("mean")
std_vector=meanFile.get("std")

filepath=args.path
f=h5py.File(filepath,"r")
cases = [ "train", "valid", "test" ]
ds = {}
for case in cases:
  print("Scaling " + case)
  train=f.get(case)
  print(train.shape)
  # get the variables which can be used for training that should be scaled
  # up to pT in flatten does not qualify
  # pick up from eta (7) to before the categories
  Info=train[:,0:7] # last number is NOT included
  tVars=train[:,7:-3] 
  cats=train[:,-3:]
  if train.shape[1] != (Info.shape[1] + tVars.shape[1] + cats.shape[1]):
    print("PROBLEM")
    exit()

  print("Training vars columns : " + str(tVars.shape[1]))
  print("Mean entries          : " + str(len(mean_vector[0])))
  print("Std entries           : " + str(len(std_vector[0])))
  if tVars.shape[1] != len(mean_vector[0]):
    print("mean_vector length not matched")
    exit()
  if tVars.shape[1] != len(std_vector[0]):
    print("std_vector length not matched")
    exit()
  tVars=(tVars-mean_vector)/std_vector

  NewTrain=np.hstack((Info,tVars,cats))
  NewTrain=np.nan_to_num(NewTrain)
  ds[case] = NewTrain

print("Done:")
for case in cases:
  print(case)
  print(ds[case].shape)

save_f = h5py.File("../../PrepareData/trainstd.h5", 'a')
save_f.create_dataset("train",data=ds["train"])
save_f.create_dataset("valid",data=ds["valid"])
save_f.create_dataset("test",data=ds["test"])



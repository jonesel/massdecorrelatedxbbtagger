import math,os,glob,h5py
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import roc_curve, roc_auc_score
import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path",dest='path',default="",help="Path to file")
parser.add_argument("--bins",dest='bins',default="16",help="Number of bins for plotting")
args = parser.parse_args()

filepath=args.path
load_file=h5py.File(filepath,'r')
train=load_file.get('train')
train=np.reshape(train,(train.shape[0],train.shape[1]))

train_QCD=train[train[:,37]==1] #Dijets
train_Higgs=train[train[:,38]==1] #Higgs

bins_mass = int(args.bins)
per_bin = int(80/bins_mass)

fig1=plt.figure()
plt.hist(train_QCD[:,5],weights=train_QCD[:,0]/np.sum(train_QCD[:,0]),bins=bins_mass,label="QCD",histtype="step")
plt.hist(train_Higgs[:,5],weights=train_Higgs[:,0]/np.sum(train_Higgs[:,0]),bins=bins_mass,label="ZPrime",histtype="step")

plt.legend(loc='upper right', fontsize="x-small")
plt.yscale("log", nonposy="clip")
plt.xlabel("Jet mass [GeV]")
plt.ylabel("Normalised Events / "+str(per_bin)+" GeV")
file1="figures/"+"Jetmass_preTraining.pdf"
fig1.savefig(file1)





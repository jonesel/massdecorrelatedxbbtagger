import argparse
import os,glob,h5py,shutil
import pandas as pd
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path", dest='path',  default="", help="path")
args = parser.parse_args()
import tensorflow as tf
from tensorflow.keras import backend as K
import tensorflow.keras as keras
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Dense, Input, Dropout, Activation, BatchNormalization
from tensorflow.keras.optimizers import SGD,Adam
import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve, roc_auc_score

model_file = args.path
model = keras.models.load_model(model_file)
test=pd.read_hdf("/storage/epp2/phrsbc/testData_concat.h5", mode="r")
fatjet=test.iloc[:,[6,7,17,23,24,26,28]] 
subjet0=test.iloc[:,[8,9,10]]
subjet1=test.iloc[:,[11,12,13]]
subjet2=test.iloc[:,[14,15,16]]
y = test.iloc[:,[37,38]] #is_Dijets, is_Hbb
w = test.iloc[:,[0,5,6,7,17,23,24,26,28]] #weight, mass, pt, eta, Split12, ZCut12, KtDR, D2, Tau21
prediction = model.predict(x=[fatjet,subjet0,subjet1,subjet1])
prediction_file="prediction_"+model_file.split("/")[5]
save_f = h5py.File("./"+prediction_file+'.h5', 'w')
predict=np.hstack((y,prediction,w))
save_f.create_dataset("predict",data=predict)
print("Prediction files for study using testing samples, they are in order:")
print("is_Dijets, is_Hbb, Prediction_ScoreQCD, Prediction_ScoreHiggs, weight, mass, pt, eta, Split12, ZCut12, KtDR, D2, Tau21")

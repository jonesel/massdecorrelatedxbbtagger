import math,os,glob,h5py
import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import roc_curve, roc_auc_score
import argparse
parser = argparse.ArgumentParser(description="%prog [options]", formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument("--path",dest='path',default="",help="Path to prediction file")
parser.add_argument("--eff",dest='eff',default="",help="Efficiency choice")
parser.add_argument("--thresh",dest='thresh',default="",help="Threshold for tagging score")
parser.add_argument("--bins",dest='bins',default="16",help="Number of bins for plotting")
args = parser.parse_args()

filepath=args.path
load_file=h5py.File(filepath,'r')
predict=load_file.get("predict")
predict=np.reshape(predict,(predict.shape[0],predict.shape[1]))

predict_QCD=predict[predict[:,0]==1] #Dijets
predict_Higgs=predict[predict[:,1]==1] #Higgs

thresh=float(args.thresh)

predict1=predict_QCD[predict_QCD[:,3]>=thresh]
predict2=predict_QCD[predict_QCD[:,3]<=thresh]
predict3=predict_Higgs[predict_Higgs[:,3]>=thresh]
predict4=predict_Higgs[predict_Higgs[:,3]<=thresh]

bins_mass = int(args.bins)
per_bin = int(80/bins_mass)
str1="Signal eff: "+args.eff

fig1=plt.figure()
# plt.hist(predict1[:,5],weights=predict1[:,4]/np.sum(predict1[:,4]),bins=bins_mass,label="QCD Pass, "+str1,histtype="step")
# plt.hist(predict_QCD[:,5],weights=predict_QCD[:,4]/np.sum(predict_QCD[:,4]),bins=bins_mass,label="QCD Total",histtype="step")
plt.hist(predict1[:,5],weights=predict1[:,4],bins=bins_mass,label="QCD Pass, "+str1,histtype="step")
plt.hist(predict_QCD[:,5],weights=predict_QCD[:,4],bins=bins_mass,label="QCD Total",histtype="step")

plt.legend(loc='upper right', fontsize="x-small")
#plt.yscale("log", nonposy="clip")
plt.xlabel("Jet mass [GeV]")
plt.ylabel("Events / "+str(per_bin)+" GeV")
file1="figures/"+"Jetmass_QCDEvents.pdf"
fig1.savefig(file1)

fig2=plt.figure()
# plt.hist(predict3[:,5],weights=predict3[:,4]/np.sum(predict3[:,4]),bins=bins_mass,label="Higgs Pass, "+str1,histtype="step",range=(70,150))
# plt.hist(predict_Higgs[:,5],weights=predict_Higgs[:,4]/np.sum(predict_Higgs[:,4]),bins=bins_mass,label="Higgs Total",histtype="step",range=(70,150))
plt.hist(predict3[:,5],weights=predict3[:,4],bins=bins_mass,label="Higgs Pass, "+str1,histtype="step",range=(70,150))
plt.hist(predict_Higgs[:,5],weights=predict_Higgs[:,4],bins=bins_mass,label="Higgs Total",histtype="step",range=(70,150))
plt.legend(loc='upper left', fontsize="x-small")
plt.xlabel("Jet mass [GeV]")
plt.ylabel("Events / "+str(per_bin)+" GeV")
file2="figures/"+"Jetmass_HiggsEvents.pdf"
fig2.savefig(file2)





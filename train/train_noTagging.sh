#!/bin/bash

#SBATCH --ntasks=1
#SBATCH --mem-per-cpu=8G
#SBATCH --output=slurm_noTagging.out
#SBATCH --error=slurm_noTagging.err

module load GCC/8.3.0 OpenMPI/3.1.4 TensorFlow/2.1.0-Python-3.7.4 matplotlib/3.1.1-Python-3.7.4 PyTorch/1.4.0-Python-3.7.4 Anaconda3

tag="select1723242628"
vars="17 23 24 26 28"
echo "I am running the training for this combination of variables: "$vars
echo "The tag is: "$tag

mkdir /storage/epp2/phrsbc/XbbOutput_noTagging/train_$tag
echo "Directory made."

python train_noTagging.py $vars $tag

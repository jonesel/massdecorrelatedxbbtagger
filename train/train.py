import sys
import argparse
import os,glob,h5py,shutil
import tensorflow as tf
from tensorflow.keras import backend as K
import tensorflow.keras as keras
from tensorflow.keras.models import Model, Sequential
from tensorflow.keras.layers import Dense, Input, Dropout, Activation, BatchNormalization
from tensorflow.keras.optimizers import SGD,Adam
import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
#from sklearn.metrics import roc_curve, roc_auc_score
from matplotlib.backends.backend_pdf import PdfPages

parser = argparse.ArgumentParser(description='Training for Xbb tagger')
parser.add_argument('variables', type=int, help='Number of variables for training in addition to pT and eta')
args = parser.parse_args()


def plot_loss(history):
  f = plt.figure()
  plt.plot(history.history['loss'], label='loss')
  plt.plot(history.history['val_loss'], label='val_loss')
  plt.xlabel('Epoch')
  plt.ylabel('Loss')
  plt.legend()
  plt.grid(True)
  return f

def define_model(params,extra_variables):
  if extra_variables == 0 : 
    kinematic_input = Input(shape=(2, ), name='fatjet') 
  else :  
    k_size = 2 + extra_variables
    kinematic_input = Input(shape=(k_size, ), name='fatjet') 
    
  subjet_1_input  = Input(shape=(3, ), name='subjet0') 
  subjet_2_input  = Input(shape=(3, ), name='subjet1') 
  subjet_3_input  = Input(shape=(3, ), name='subjet2')        
  inputs = [kinematic_input, subjet_1_input, subjet_2_input, subjet_3_input]
  concatenated_inputs = keras.layers.concatenate(inputs)

  for i in range(params['num_layers']):
    if i==0:
      x = Dense(params['num_units'], kernel_initializer='orthogonal')(concatenated_inputs)
      if params['batch_norm']:
        x = BatchNormalization()(x)
      x = Activation(params['activation_type'])(x)
      if params['dropout_strength'] > 0:
        x = Dropout(params['dropout_strength'])(x)
    else:
      x = Dense(params['num_units'], kernel_initializer='orthogonal')(x)
      if params['batch_norm']:
        x = BatchNormalization()(x)
      x = Activation(params['activation_type'])(x)
      if params['dropout_strength'] > 0:
        x = Dropout(params['dropout_strength'])(x)

  predictions = Dense(params['output_size'], activation='softmax', kernel_initializer='orthogonal')(x)
  model = Model(inputs=inputs, outputs=predictions)
  adm = Adam(lr=params['learning_rate'], decay=params['lr_decay'])
  model.compile(loss='categorical_crossentropy', optimizer=adm)
  return model


print("************ PHASE LOAD FILE *****************")
train_file=h5py.File("/storage/epp2/phrsbc/trainstd.h5","r")
extra_variables = args.variables

train=train_file.get("train")
valid=train_file.get("valid")

# weight position 0
# jet pT position 6 
# jet eta position 7
# subjet pb, pc, pl x 3 : 8-16
if extra_variables == 0 : 
  fatjet = train[:, [6, 7]]
  fatjetV = valid[:, [6, 7]]

else : 
  i = 17
  final_i = i+extra_variables
  indices = [ 6, 7 ]
  while i < final_i :
    indices.append(i)
    i+=1
  fatjet = train[:, indices]
  fatjetV = valid[:, indices]

print(fatjet)
    
subjet0=train[:, [ 8, 9,10]]
subjet1=train[:, [11,12,13]]
subjet2=train[:, [14,15,16]]
train_y=train[:, [37,38]] # get last 2 categories - not using top
train_w=train[:,  0 ] # use weight - should RW to flat distribution in pT

# validation data
subjet0V=valid[:, [ 8, 9,10]]
subjet1V=valid[:, [11, 12, 13]]
subjet2V=valid[:, [14, 15, 16]]
valid_y=valid[:,  [37, 38]]
valid_w=valid[:,   0 ] # use weight - should RW to flat distribution in pT


print("************ PHASE TRAINING *****************")
nEpochs = 100  # CHANGE TO 200 eventually?
params = {'num_layers': 6,'num_units': 250,'activation_type': 'relu','dropout_strength': 0.,'learning_rate': 0.01,'lr_decay': 0.00001,'epochs': nEpochs,'batch_norm': True,'output_size': 2,}   # output size is only 2 since not using top samples

model = define_model(params,extra_variables)
model_name ="_AdmStd_DL1r"
save_path = "/storage/epp2/phrsbc/XbbOutput_100epochs/train_"+str(extra_variables)+"/"
batchsize=10000
save_best = keras.callbacks.ModelCheckpoint(filepath=save_path + model_name + "_best.h5", monitor='val_loss', verbose=0, save_best_only=True)
early_stopping = keras.callbacks.EarlyStopping(monitor='val_loss', min_delta=0, patience=20)
csv_logger = keras.callbacks.CSVLogger(save_path + model_name + '.log')
reduce_lr_on_plateau = keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.5, patience=10, verbose=0, mode='auto', epsilon=0.0001, cooldown=0, min_lr=0)
callbacks = [save_best, csv_logger, reduce_lr_on_plateau]
history = model.fit(x=[fatjet,subjet0,subjet1,subjet2],y=train_y,sample_weight=train_w,validation_data=([fatjetV,subjet0V,subjet1V,subjet2V],valid_y,valid_w),batch_size=batchsize,callbacks=callbacks,epochs = params['epochs'])
print("History")
print(history)

arch = model.to_json()
with open(save_path + model_name + '_architecture.json', 'w') as arch_file:
        arch_file.write(arch)
model.save_weights(save_path + model_name + '_weights.h5')
model.save(save_path + model_name + '.h5')

pdf = PdfPages(save_path+"training_"+model_name+".pdf")
lossPlot = plot_loss(history)
pdf.savefig( lossPlot ) 
pdf.close()









#!/bin/bash

i="0"

while [ $i -lt 15 ] #14 extra variables! 
do

filename="Train_"${i}".sh"
echo filename=$filename
rm -fr $filename

touch $filename
cat<<EOF >$filename
#!/bin/bash
#SBATCH --ntasks=1
#SBATCH --mem= 2048
#SBATCH --output=/storage/epp2/phrsbc/XbbOutput/
#SBATCH --partition=epp 

module load GCC/8.3.0 OpenMPI/3.1.4 TensorFlow/2.1.0-Python-3.7.4 matplotlib/3.1.1-Python-3.7.4 PyTorch/1.4.0-Python-3.7.4 Anaconda3

echo "I am running the training for 16+"$i" variables"

mkdir /storage/epp2/phrsbc/XbbOutput_100epochs/train_$i/

python train.py $i

rm -f $PWD/$filename 
 
EOF

chmod +x $filename
sbatch -p epp $PWD/$filename

i=$[$i+1]

done
